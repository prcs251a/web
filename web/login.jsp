<%@ page import="dao.CountiesDao" %>
<%@ page import="models.County" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="head.jsp" %>
    <title>Login</title>
</head>
<body>
<%@ include file="nav-bar.jsp" %>
    <div class="main-content-small" id="login-register-content" style="padding-right:20px; padding-left:20px; padding-top:10px;">
        <h3>Login</h3>
            <div class="tab-pane fade in " id="tab1default">
                <form action="loginprocess.jsp" method="post">
                    <div class="input-group">
                        <span class="input-group-addon">Username</span>
                        <input type="text" name="username" class="form-control" placeholder="Username" aria-describedby="basic-addon1" required>
                    </div>


                    <div class="input-group">
                        <span class="input-group-addon">Password</span>
                        <input type="password" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" required>
                    </div>
                    <input type="hidden" name="refUrl" value="<%=request.getHeader("referer")%>" />

                    <input type="submit" value="Login">
                </form>
            </div>
    </div>
</body>
</html>
