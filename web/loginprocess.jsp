<%@ page import="dao.CustomersDao" %>
<%@ page import="models.User" %>
<%@ page import="models.Customer" %>
<jsp:useBean id="obj" class="login.LoginDetails"/>
<jsp:setProperty property="*" name="obj"/>
<jsp:useBean id="currentUser" class="models.User" scope="session"/>
<jsp:useBean id="currentCustomer" class="models.Customer" scope="session"/>


<%
    String url;
    url = request.getHeader("referer");
    boolean status = CustomersDao.validate(obj);
    if (status){
        User user = CustomersDao.getUserByUsername(obj.getUsername());
        Customer customer = CustomersDao.getCustomerByID(user.getCustomerID());
%>
        <!-- User Bean-->
        <jsp:setProperty name="currentUser" property="username" value="<%=user.getUsername()%>"/>
        <jsp:setProperty name="currentUser" property="userID" value="<%=user.getUserID()%>"/>
        <jsp:setProperty name="currentUser" property="email" value="<%=user.getEmail()%>"/>
        <!-- Customer Bean -->
        <jsp:setProperty name="currentCustomer" property="customerID" value="<%=customer.getCustomerID()%>"/>
        <jsp:setProperty name="currentCustomer" property="customerEmail" value="<%=customer.getCustomerEmail()%>"/>
        <jsp:setProperty name="currentCustomer" property="customerFirstName" value="<%=customer.getCustomerFirstName()%>"/>
        <jsp:setProperty name="currentCustomer" property="customerLastName" value="<%=customer.getCustomerLastName()%>"/>
        <jsp:setProperty name="currentCustomer" property="postcode" value="<%=customer.getPostcode()%>"/>
        <jsp:setProperty name="currentCustomer" property="city" value="<%=customer.getCity()%>"/>
        <jsp:setProperty name="currentCustomer" property="addressLine1" value="<%=customer.getAddressLine1()%>"/>
        <jsp:setProperty name="currentCustomer" property="addressLine2" value="<%=customer.getAddressLine2()%>"/>
        <jsp:setProperty name="currentCustomer" property="countyID" value="<%=customer.getCountyID()%>"/>
<%
        session.setAttribute("session","TRUE");
    }
    else
    {
    }
%>
<meta http-equiv="Refresh" content="0;url=<%=url%>">
