<html>
<head>
	<%@ include file="head.jsp" %>

</head>

<!---------------Nav Bar------------------------->
<%@ include file="nav-bar.jsp" %>
<!---------------Nav Bar------------------------->
<h3 id="search-term-header" style="text-align:center">Search Results: <%=request.getParameter("q")%></h3>

<div class="search-results-wrapper">
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#artist-search">Artist</a></li>
		<li><a data-toggle="tab" href="#tour-search">Tour</a></li>
		<li><a data-toggle="tab" href="#venue-search">Venue</a></li>
	</ul>
	<div class="tab-content">
		<div id="artist-search" class="tab-pane fade in active">
			<ul class="search-results-ul">
			<c:forEach var="artist" items="${artists}">
					<li class="event-li">
						<div class="event-thumbnail-div">
							<img class="event-image-thumb" src="images/thumbs/${artist.imageID}.jpg">
						</div>
							<div class="event-description-div">
								${artist.artistName}
								<hr style="margin-right:5px"></hr>
								<input class = "event-select-button" type="button" name = "view-tickets" value="View Tickets" onclick="location.href='artist?id=${artist.artistId}';">
								<p>
									${artist.artistDescription}
								</p>
							</div>
				</li>
			</c:forEach>
			</ul>
		</div>
		<div id="tour-search" class="tab-pane fade">
			<ul class="search-results-ul">
				<c:forEach var="tour" items="${tours}">
					<li class="event-li" onclick="">
						<div class="event-thumbnail-div">
							<img class="event-image-thumb" src="images/${tour.imageID}-thumbnail.jpg">
						</div>
						<div class="event-description-div">
								${tour.tourName}
							<hr style="margin-right:5px"></hr>
							<input class = "event-select-button" type="button" name = "view-tickets" value="View Tickets" onclick="location.href='tour?id=${tour.tourID}';">
							<p>
									${tour.tourDescription}
							</p>
						</div>
					</li>
				</c:forEach>
			</ul>
		</div>
		<div id="venue-search" class="tab-pane fade">
			<ul class="search-results-ul">
				<c:forEach var="venue" items="${venues}">
					<li class="event-li">
						<div class="event-thumbnail-div">
							<img class="event-image-thumb" src="images/${venue.imageID}-thumbnail.jpg">
						</div>
						<div class="event-description-div">
								${venue.venueName}
							<hr style="margin-right:5px"></hr>
							<input class = "event-select-button" type="button" name = "view-tickets" value="View Tickets" onclick="location.href='venue?id=${venue.venueID}';">
							<p>
									${venue.venueDescription}
							</p>
						</div>
					</li>
				</c:forEach>
			</ul>
		</div>
	</div>
</div>

</body>
</html>