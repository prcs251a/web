<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="head.jsp" %>
    <script>
        function showDiv(elem) {
            if (elem.value == 0 || elem.value == 1){
                document.getElementById('security-code-div').style.display = "table";
                document.getElementById('paypal-button-div').style.display = "none";
            }else{
                document.getElementById('security-code-div').style.display = "none";
                document.getElementById('paypal-button-div').style.display = "inline-block";
            }
        }
    </script>
    <title>Checkout</title>
</head>
<body>
<%@ include file="nav-bar.jsp" %>
<div style="width:100%;">
    <div class="checkout-login" style="width: 49%; background: rgb(238, 238, 238) none repeat scroll 0% 0%; height: 100%; padding: 20px; float: left;">
        <h3>Existing Users</h3>
        <div class="tab-pane fade in " id="tab1default">
            <form action="loginprocess.jsp" method="post">
                <div class="input-group">
                    <span class="input-group-addon">Username</span>
                    <input type="text" name="username" class="form-control" placeholder="Username" aria-describedby="basic-addon1" required>
                </div>


                <div class="input-group">
                    <span class="input-group-addon">Password</span>
                    <input type="password" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" required>
                </div>
                <input type="hidden" name="refUrl" value="<%=request.getHeader("referer")%>" />
                <input type="submit" value="Login">
            </form>
        </div>
    </div>
    <div class="checkout-guest" style="float: right; background: rgb(238, 238, 238) none repeat scroll 0% 0%; width: 49%; padding: 20px; height: 100%;">
        <h3>Guest Checkout</h3>
        <form method="post" data-toggle="validator" role="form" disable="true">
            <div class="form-group has-feedback">
                <label for="first-name" class="control-label">First Name</label>
                <div class="input-group">
                    <span for="first-name" class="input-group-addon">First Name</span>
                    <input type="text" id="first-name" class="form-control" placeholder="First Name" name="firstName" aria-describedby="basic-addon1" required>
                </div>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="last-name" class="control-label">Last Name</label>
                <div class="input-group">
                    <span for="last-name" class="input-group-addon">Last Name</span>
                    <input type="text" id="last-name" class="form-control" placeholder="Last Name" name="lastName" aria-describedby="basic-addon1" required>
                </div>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group">
                <label for="county" class="control-label">County</label>

                <select id="county" class="form-control" name="county" required>
                    <option value=""> </option>
                    <c:forEach var="county" items="${counties.counties}">
                        <option value="${county.countyID}">${county.countyName}</option>
                    </c:forEach>
                </select>

            </div>
            <div class="form-group has-feedback">
                <label for="city" class="control-label">Last Name</label>
                <div class="input-group">
                    <span for="city" class="input-group-addon">Last Name</span>
                    <input type="text" id="city" class="form-control" placeholder="City" name="city" aria-describedby="basic-addon1" required>
                </div>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="addressLine1" class="control-label">Address Line One</label>
                <div class="input-group">
                    <span for="addressLine1" class="input-group-addon">Address</span>
                    <input type="text" id="addressLine1" class="form-control" placeholder="Address Line One" name="addressLine1" aria-describedby="basic-addon1" required>
                </div>
            </div>
            <div class="form-group">
                <label for="addressLine2" class="control-label">Address Line Two</label>
                <div class="input-group">
                    <span for="addressLine2" class="input-group-addon">Address</span>
                    <input type="text" id="addressLine2" class="form-control" placeholder="Address Line Two" name="addressLine2" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="form-group has-feedback">
                <label for="postCode" class="control-label">Postcode</label>
                <div class="input-group">
                    <span for="postCode" class="input-group-addon">Postcode</span>
                    <input type="text" id="postCode" class="form-control" placeholder="Postcode" name="postCode" aria-describedby="basic-addon1"
                           pattern="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][¿¿0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$" style="text-transform: uppercase" required>
                </div>
            </div>
            <div class="form-group has-feedback">
                <label for="email1" class="control-label">Email</label>
                <div class="input-group">
                    <span for="email1" class="input-group-addon">Email</span>
                    <input type="text" id="email1" class="form-control" placeholder="Email" name="email1" aria-describedby="basic-addon1"
                           pattern="^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$" required>
                </div>
            </div>

            <div class="form-group has-feedback">
                <label for="email2" class="control-label">Confirm Email</label>
                <div class="input-group">
                    <span for="email2" class="input-group-addon">Email</span>
                    <input type="text" id="email2" class="form-control" placeholder="Confirm Email" name="email2" aria-describedby="basic-addon1"
                           pattern="^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$" data-match="#email1" data-match-error="Whoops, these don't match" required>
                </div>
                <div class="help-block with-errors"></div>
            </div>
            <input type="submit" value="Checkout">
        </form>
    </div>
</div>
</body>
</html>

