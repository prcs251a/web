<html>
<head>
    <%@ include file="head.jsp" %>
    <title>Artist - <c:out value="${tour.tourName}"/></title>
</head>
<body>
<%@ include file="nav-bar.jsp" %>
<div class="tour-content">
    <h3><c:out value="${tour.tourName}"/></h3>
    <h4></h4>
    <div class="tour-banner">
        <div class="tour-image-area">
            <img class="tour-image" src="images/banners/${tour.imageID}.jpg">
        </div>
        <p>
            <c:out value="${tour.tourDescription}"/>
        </p>
    </div>
</div>

</br>

<div class="order-history-content">
    <table class="artist-events-table">
        <tr>
            <th>
                Tour Name
            </th>
            <th>
                Event Date
            </th>
            <th>
                Artist
            </th>
            <th>
                Venue
            </th>
            <th>

            </th>
        </tr>
            <c:forEach var="event" items="${events}">
                <tr>
                    <td>
                        ${tour.tourName}
                    </td>
                    <td>
                        ${event.eventDate}
                    </td>
                    <td>
                        ${artist.artistName}
                    </td>
                    <td>
                        ${event.venue.venueName}
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${event.soldOut == true}">
                                <button class="btn btn-danger btn-block">Unavailable </button>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${event.pastEvent == false}">
                                        <button class="btn btn-success btn-block" onclick="location.href='event?id=${event.eventID}';">View Tickets</button>
                                    </c:when>
                                    <c:otherwise>
                                        <button class="btn btn-danger btn-block">Unavailable </button>
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>
