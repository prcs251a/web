<html>
<head>
    <%@ include file="head.jsp" %>
    <title>Artist - <c:out value="${artist.artistName}"/></title>
</head>
<body>
<%@ include file="nav-bar.jsp" %>
<div class="tour-content">
    <h3><c:out value="${artist.artistName}"/></h3>
    <h4></h4>
    <div class="tour-banner">
        <div class="tour-image-area">
            <img class="tour-image" src="images/${tour.imageID}-banner.jpg">
        </div>
        <p>
            <c:out value="${artist.artistDescription}"/>
        </p>
    </div>
</div>
    </br>
    <div class="order-history-content">
        <table class="artist-events-table">
            <tr>
                <th>
                    Tour Name
                </th>
                <th>
                    Event Date
                </th>
                <th>
                    Seating Tier
                </th>
                <th>
                    Remaining Tickets
                </th>
                <th>

                </th>
            </tr>
            <tr style="cursor:pointer">
                <td>
                    ${tour.tourName}
                </td>
                <td>
                    ${event.eventDate}
                </td>
                <td>
                    Standing
                </td>
                <td>
                    ${event.standingTickets}
                </td>
                <td>
                    <c:choose>
                        <c:when test="${event.secondTierTickets > 0}">
                            <form class="table-button-form" method="post">
                                <select name="quantity">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                                <input type="hidden" name="event" value="${event.eventID}">
                                <input type="hidden" name="tier" value="1">
                                <button class="btn btn-success" type="submit">Add to Basket</button>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <button class="btn btn-danger btn-block">Unavailable </button>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr style="cursor:pointer">
                <td>
                    ${tour.tourName}
                </td>
                <td>
                    ${event.eventDate}
                </td>
                <td>
                    Standard Seating
                </td>
                <td>
                    ${event.firstTierTickets}
                </td>
                <td>
                    <c:choose>
                        <c:when test="${event.firstTierTickets > 0}">
                            <form class="table-button-form" method="post">
                                <select name="quantity">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                                <input type="hidden" name="event" value="${event.eventID}">
                                <input type="hidden" name="tier" value="2">
                                <button class="btn btn-success" type="submit">Add to Basket</button>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <button class="btn btn-danger btn-block">Unavailable </button>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr style="cursor:pointer">
                <td>
                    ${tour.tourName}
                </td>
                <td>
                    ${event.eventDate}
                </td>
                <td>
                    Premium Seating
                </td>
                <td>
                    ${event.secondTierTickets}
                </td>
                <td>
                    <c:choose>
                        <c:when test="${event.secondTierTickets > 0}">
                            <form class="table-button-form" method="post">
                                <select name="quantity">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                                <input type="hidden" name="event" value="${event.eventID}">
                                <input type="hidden" name="tier" value="3">
                                <button class="btn btn-success" type="submit">Add to Basket</button>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <button class="btn btn-danger btn-block">Unavailable </button>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            </tbody>
        </table>
    <br/>
</div>

</body>
</html>
