<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="head.jsp" %>
</head>
<body onload="loadAuto()">
<%@ include file="nav-bar.jsp" %>

<div class="order-history-content" style="text-align: left">
    <h3 style="text-align: center">Basket Items</h3>
    <table class="ticket-history-table">
        <tr>
            <th>
                Event ID
            </th>
            <th>
                Tier
            </th>
            <th>
                Quantity
            </th>
            <th>
                Edit
            </th>
            <th>
                Delete
            </th>
        </tr>
        <c:forEach var="item" items="${thisBasket.basketItems}">
        <tr>
            <td>
                ${item.tourName}
            </td>
            <td>
                ${item.tierName}
            </td>
            <td>
                ${item.quantity}
            </td>
            <td>
                <form method="post" data-toggle="validator" role="form">
                <input hidden name="action" value="quantity">
                <select name="quantity">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
                    <input type="hidden" name="item" value="${item.itemID}">
                <button class="btn-success" type="submit" value="Change">Change</button>
                </form>
            </td>
            <td>
                <form method="post" role="form"/>
                    <input hidden name="action" value="remove"/>
                    <input type="hidden" name="item" value="${item.itemID}"/>
                    <button class="btn-danger" type="submit" value="Remove">Remove</button>
                </form>
            </td>
        </tr>
        </c:forEach>
    </table>
    <div style="margin-right: auto; margin-left: auto; width:160px;">
    <button style="margin-top:5px; margin-right:auto; margin-left: auto;" class="btn btn-info" onclick="location.href='checkout'">Proceed to Checkout</button>
    </div>
    </div>
</body>
</html>
