<%@page contentType="text/html" pageEncoding="UTF-8" %>
<html>
<head>
  <%@ include file="head.jsp" %>
</head>
<body onload="loadAuto()">
<%@ include file="nav-bar.jsp" %>
<h3 style="text-align:center">My Tickets: Order History</h3>
<div class="order-history-content">
  <table class="ticket-history-table">
    <tr>
      <th>
        Order Number
      </th>
      <th>
        Order Date
      </th>
      <th>
        Tour Name
      </th>
      <th>
        Seating Tier
      </th>
        <th>
         Total Cost
        </th>
    </tr>
    <c:forEach var="history" items="${histories}">
    <tbody>
      <tr data-toggle="collapse" data-target=".ticket-${history.booking.bookingID}" class="clickable parent" style="cursor:pointer">
      <td>
          ${history.booking.bookingID}
      </td>
      <td>
          ${history.booking.datePurchased}
      </td>
      <td>
          ${history.tourName}
      </td>
      <td>
          #
      </td>
      <td>
          £${history.booking.totalCost}
      </td>
    </tr>
      <c:forEach var="ticket" items="${history.tickets}">
          <tr class="collapse ticket-${history.booking.bookingID} child" style="background:#777777">
              <td>#</td>
              <td>#</td>
              <td>${history.tourName}</td>
              <td>${ticket.tierName}</td>
              <td>${ticket.ticketPrice}</td>
          </tr>
          </tbody>
      </c:forEach>
    </c:forEach>

  </table>
</div>
</div>

</body>
<html>
