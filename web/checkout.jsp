<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="head.jsp" %>
    <script>
        function showDiv(elem) {
            if (elem.value == 0 || elem.value == 1){
                document.getElementById('security-code-div').style.display = "table";
                document.getElementById('paypal-button-div').style.display = "none";
            }else{
                document.getElementById('security-code-div').style.display = "none";
                document.getElementById('paypal-button-div').style.display = "inline-block";
            }
        }
    </script>
    <title>Checkout</title>
</head>
<body>
    <%@ include file="nav-bar.jsp" %>
    <div class="order-history-content" style="align-content: center; text-align: center;">
        <h3>Basket Items</h3>
        <table class="ticket-history-table">
            <tr>
                <th>
                    Event ID
                </th>
                <th>
                    Tier
                </th>
                <th>
                    Quantity
                </th>
                <th>
                    Edit
                </th>
                <th>
                    Delete
                </th>
            </tr>
            <c:forEach var="item" items="${thisBasket.basketItems}">
            <tr>
                <td>
                        ${item.tourName}
                </td>
                <td>
                        ${item.tierName}
                </td>
                <td>
                        ${item.quantity}
                </td>
                <td>
                    <form method="post" data-toggle="validator" role="form">
                        <input hidden name="action" value="quantity">
                        <select name="quantity">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                        <input type="hidden" name="item" value="${item.itemID}">
                        <button class="btn-success" type="submit" value="Change">Change</button>
                    </form>
                </td>
                <td>
                    <form method="post" role="form"/>
                    <input hidden name="action" value="remove"/>
                    <input type="hidden" name="item" value="${item.itemID}"/>
                    <button class="btn-danger" type="submit" value="Remove">Remove</button>
                    </form>
                </td>
            </tr>
            </c:forEach>
            </table>
        <div class="main-content-small">
            <h3>Payment Option</h3>
            <select id="paymentMethod" class="form-control" name="" onchange="showDiv(this)" required>
                <option value="0">Example Credit Card **** **** **** 2345</option>
                <option value="1">Example Debit Card **** **** **** 2246</option>
                <option value="2">Paypal</option>
            </select>
            <form method="post" role="form">
            <div class="form-group has-feedback" id="security-code-div" style="align-content: center; width:100%;  padding-top:20px;">
                <div class="input-group" style="padding-left: 15%; width:100%;">
                    <span for="securityCode" class="input-group-addon">Security Code</span>
                    <input type="text" id="securityCode" class="form-control" placeholder="" style="width:80px;" name="securityCode" aria-describedby="basic-addon1" required>
                    <input type="hidden" name="action" value="purchase">
                    <button type="submit" class ="btn btn-info">Purchase</button>
                </div>
            </div>
            </form>
            <form method="post" role="form">
                <div class="form-group has-feedback" id="paypal-button-div" style="display: none; padding-top:20px;">
                    <input type="hidden" name="action" value="purchase">
                    <button type="submit" class ="btn btn-info">Purchase With Paypal</button>
                </div>
            </form>
        </div>
    </div>

</body>
</html>
