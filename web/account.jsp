<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="head.jsp" %>
    <title>My Account</title>
</head>
<body>
<%@ include file="nav-bar.jsp" %>

<div class ="main-content-small" id = "account-content" style = "padding-right:20px; padding-left:20px;">
    <h3>Account</h3>
    <form method = "post" data-toggle="validator" role="form" disable = "true">
        <input type="hidden" name="action" value="accountEdit">
        <div class = "form-group has-feedback">
            <label>Username</label>
            <div class = "input-group">
                <span class = "input-group-addon">Username</span>
                <label type = "text" id = "username" class = form-control aria-describedby="basic-addon1" required>
                    <jsp:getProperty name="currentUser" property="username"/>
                </label>
            </div>
        </div>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class = "form-group has-feedback">
            <label>First Name</label>
            <div class = input-group>
                <span class = "input-group-addon">First Name</span>
                <input type="text" id="first-name" value = "<%=currentCustomer.getCustomerFirstName()%>" class="form-control" placeholder="First Name" name="firstName" aria-describedby="basic-addon1" required>

            </div>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label for="last-name" class="control-label">Last Name</label>
            <div class="input-group">
                <span for="last-name" class="input-group-addon">Last Name</span>
                <input type="text" id="last-name" value = "<%=currentCustomer.getCustomerLastName()%>" class="form-control" placeholder="Last Name" name="lastName" aria-describedby="basic-addon1" required>
            </div>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group">
            <label for="county" class="control-label">County</label>

            <select id="county" class="form-control" name="county"  required>
                <c:forEach var="county" items="${counties.counties}">
                    <option value="${county.countyID}">${county.countyName}</option>
                </c:forEach>
                <option selected value=""> </option>

            </select>
        </div>
        <div class="form-group has-feedback">
            <label for="city" class="control-label">City</label>
            <div class="input-group">
                <span for="city" class="input-group-addon">City</span>
                <input type="text" id="city" value="<%=currentCustomer.getCity()%>" class="form-control"  placeholder="City" name="city" aria-describedby="basic-addon1" required>
            </div>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label for="addressLine1" class="control-label">Address Line One</label>
            <div class="input-group">
                <span for="addressLine1" class="input-group-addon">Address</span>
                <input type="text" id="addressLine1" value="<%=currentCustomer.getAddressLine1()%>" class="form-control" placeholder="Address Line One" name="addressLine1" aria-describedby="basic-addon1" required>
            </div>
        </div>
        <div class="form-group">
            <label for="addressLine2" class="control-label">Address Line Two</label>
            <div class="input-group">
                <span for="addressLine2" class="input-group-addon">Address</span>
                <input type="text" id="addressLine2" value="<%=currentCustomer.getAddressLine2()%>" class="form-control" placeholder="Address Line Two" name="addressLine2" aria-describedby="basic-addon1">
            </div>
        </div>
        <div class="form-group has-feedback">
            <label for="postCode" class="control-label">Postcode</label>
            <div class="input-group">
                <span for="postCode" class="input-group-addon">Postcode</span>
                <input type="text" id="postCode" value="<%=currentCustomer.getPostcode()%>" class="form-control" placeholder="Postcode" name="postCode" aria-describedby="basic-addon1"
                       pattern="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][¿¿0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$" style="text-transform: uppercase" required>
            </div>
        </div>
        <div class="form-group has-feedback">
            <label for="email1" class="control-label">Email</label>
            <div class="input-group">
                <span for="email1" class="input-group-addon">Email</span>
                <input type="text" id="email1" value="<%=currentCustomer.getCustomerEmail()%>" class="form-control" placeholder="Email" name="email1" aria-describedby="basic-addon1"
                       pattern="^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$" required>
            </div>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group has-feedback">
            <label for="email2" class="control-label">Confirm Email</label>
            <div class="input-group">
                <span for="email2" class="input-group-addon">Email</span>
                <input type="text" id="email2" value="<%=currentCustomer.getCustomerEmail()%>" class="form-control" placeholder="Confirm Email" name="email2" aria-describedby="basic-addon1"
                       pattern="^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$" data-match="#email1" data-match-error="Whoops, these don't match" required>
            </div>
            <div class="help-block with-errors"></div>
        </div>
        <button type="submit" class ="btn btn-info" value = "Update">Update All</button>
        </form>

        <form method="post" data-toggle="validator" role="form" disable = "true">
        <div class = "form-group has-feedback">
            <div class = "form-group has-feedback">
                <button type="button" class="btn btn-info" data-toggle ="collapse" data-target = #collapse >Reset Password</button>
                <div id = collapse class="collapse">
                    <div class="form-group has-feedback">
                        <label for="password1" class="control-label">Current Password</label>
                        <div class="input-group">
                            <span for="password1" class="input-group-addon">Password</span>
                            <input type="password" id="password1" class="form-control" placeholder="Password" name="currentPassword" aria-describedby="basic-addon1" required>
                        </div>
                    </div>


                    <div class="form-group has-feedback">
                        <label for="newPassword" class="control-label">New Password</label>
                        <div class="input-group">
                            <span for = "newPassword" class = "input-group-addon">Password</span>
                            <input type="password" id="newPassword" class="form-control" placeholder="New Password" name="newPassword" aria-describedby="basic-addon1"
                                   pattern="^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@%#]).{8,20})$"
                                   required>
                        </div>
                        <p>A password must be between 8 and 20 characters and have at least one upper case letter, one lower case letter, one number and one of the following;!@%#</p>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="password2" class="control-label">Confirm New Password</label>
                        <div class="input-group">
                            <span for="password2" class="input-group-addon">Password</span>
                            <input type="password" id="password2" class="form-control" placeholder="Confirm Password" name="confirmNewPassword" aria-describedby="basic-addon1" data-match="#newPassword" data-match-error="Whoops, these don't match"
                                   pattern="^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@%#]).{8,20})$"
                                   required>

                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <input type="hidden" name="action" value="passwordChange"/>
                    <button type="submit" class ="btn btn-info" value = "Change">Change</button>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>
