<%@ page import="models.SeatingTier" %>
<%@ page import="models.Event" %>
<%@ page import="models.Booking" %>
<%@ page import="models.Basket" %>
<%@ page import="models.BasketItem" %>
<%@ page import="models.Ticket" %>
<%@ page import="dao.*" %>
<jsp:useBean id="currentCustomer" class="models.Customer" scope="session"/>
<jsp:useBean id="currentBasket" class="models.Basket" scope="session"/>
<jsp:useBean id="currentUser" class="models.User" scope="session"/>
<%
    Booking booking = new Booking();
    if(currentUser.getUsername() == null){
        booking.setCustomerID(currentCustomer.getCustomerID());
    }else {
        booking.setCustomerID(currentCustomer.getCustomerID());
    }
    booking.setBookingID(BookingsDao.getNextBookingID());
    booking.setTotalCost(0f);

    boolean ticketStatus = false;

    for(BasketItem item : currentBasket.getBasketItems()){
        SeatingTier tier = SeatingTiersDao.getSeatingTierByID(item.getTier());
        booking.setTotalCost(booking.getTotalCost()+tier.getTicketPrice());
    }

    boolean bookingStatus = BookingsDao.createBooking(booking);

    for(BasketItem item : currentBasket.getBasketItems()){
        for(int i = 0; i < item.getQuantity(); i++) {
            Ticket ticket = new Ticket();
            ticket.setTicketID(TicketDao.getNextTicketID());
            ticket.setTierID(item.getTier());
            ticket.setBookingID(booking.getBookingID());
            ticket.setEventID(item.getEventID());
            ticketStatus = TicketDao.createTicket(ticket);
        }
    }

    if(bookingStatus && ticketStatus){
        currentBasket = new Basket();
%>
<br/>
<img src="images/material-tick.png" width="20%" height="40%" style="display: block; margin-left: auto; margin-right: auto"/>
<br/>
<p style="text-align: center;font-family: Calibri;font-size: 30pt" >Success!</p>
<%
}else{
%>
<a>Error booking your ticket</a>
<%
    }
%>
<meta http-equiv="Refresh" content="3;url=index">
