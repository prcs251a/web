<%@ page import="models.BasketItem" %>
<%@ page import="dao.*" %>
<jsp:useBean id="currentUser" class="models.User" scope="session"/>
<jsp:useBean id="currentBasket" class="models.Basket" scope="session"/>
<jsp:useBean id="counties" class="models.Counties" scope="application"/>
<jsp:useBean id="currentCustomer" class="models.Customer" scope="session"/>
<%
    if(counties.getCounties().size() == 0)counties.setCounties(CountiesDao.getCounties());
%>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%=request.getContextPath()%>/">Ticket Company</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">

                <%
                    if (currentUser == null || currentUser.getUsername() == null){
                %>
                <li class="dropdown" id="menuLogin">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin"><span class="glyphicon glyphicon-user" aria-hidden="true"/> Log In</a>
                    <div class="dropdown-menu" style="padding:17px;">
                        <form class="form" id="formLogin" action="loginprocess.jsp" method="post">
                            <input name="username" id="username" placeholder="Username" type="text">
                            <input name="password" id="password" placeholder="Password" type="password"><br>
                            <button type="submit" value="Register">Login</button>

                        </form>
                        <button onclick="location.href='register'">Register</button>
                    </div>
                </li>
                <%} else {
                %>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user" aria-hidden="true"/> <%=currentUser.getUsername()%><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="my-tickets">My Tickets</a></li>
                        <li><a href="account">Account</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="logoutprocess.jsp">Logout</a></li>
                    </ul>
                </li>
                <%
                    }
                %>
            </ul>

            <ul class="nav navbar-nav navbar-right">

                        <%
                            if(currentBasket != null){
                        %>
                <li class = dropdown><a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navBasket"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> <%=currentBasket.getBasketItems().size()%></a>
                    <div class="dropdown-menu">
                        <table class="ticket-history-table">
                            <tr>
                                <th>
                                    Event ID
                                </th>
                                <th>
                                    Tier
                                </th>
                                <th>
                                    Quantity
                                </th>
                            </tr>
                            <%
                                for(BasketItem basketItem : currentBasket.getBasketItems()){
                            %>
                            <tr>
                                <td>
                                    <%=TourDao.getTourByID(EventDao.getEventByID(basketItem.getEventID()).getTourID()).getTourName()%>
                                </td>
                                <td>
                                    <%=SeatingTiersDao.getSeatingTierByID(basketItem.getTier()).getTierName()%>
                                </td>
                                <td>
                                    <%=basketItem.getQuantity()%>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                        </table>
                        <button style="margin-top:5px; margin-right:auto; margin-left: auto;" onclick="location.href='basket'">Go To Checkout</button>
                        <%
                            }else{

                        %>
                <li class = dropdown><a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navBasket"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></a>
                    <div class="dropdown-menu">
                <%
                    }
                %>
                    </div>
                </li>

            </ul>

            <form class="navbar-form navbar-right" role="search" action="search-results" method="get">
                <div class="form-group">
                    <input type="text" id="nav-search" class="form-control" name="q" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>