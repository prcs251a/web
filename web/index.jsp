<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
  <%@ include file="head.jsp" %>
</head>
<body onload="loadAuto()" style="overflow: hidden;">
<%@ include file="nav-bar.jsp" %>
<section class="slider">
  <div>
    <c:forEach var="artist" items="${artists}">
      <div onclick="location.href='artist?id=${artist.artistId}';">${artist.artistName}<img src="images/banners/${artist.imageID}.jpg" width="100%" height="100%"/>${artist.artistName}</div>
    </c:forEach>
  </div>
</section>
<div class="index-listing-wrapper">
  <div class="index-listing-div">
    <h6 class="margin-bottom:0px;">Popular Artists</h6>
      <ul class="index-listing">
              <c:forEach var="artist" items="${selectedArtists}">
                <li class="index-listing-li">
                  <div class="event-thumbnail-div">
                    <img class="event-image-thumb" src="images/thumbs/${artist.imageID}.jpg">
                  </div>
                  <div class="event-description-div">
                      ${artist.artistName}
                    <hr style="margin-right:5px; margin-top: 0px; margin-bottom: 0px;"></hr>
                    <input class = "event-select-button" type="button" name = "view-tickets" value="View Tickets" onclick="location.href='artist?id=${artist.artistId}';">
                    <p>
                        ${artist.artistDescription}
                    </p>
                  </div>
                </li>
              </c:forEach>
      </ul>
  </div>
  <div class="index-listing-div">
    <h6 class="margin-bottom:0px;">Popular Tours</h6>
    <ul class="index-listing">
      <c:forEach var="tour" items="${tours}">
        <li class="index-listing-li">
          <div class="event-thumbnail-div">
            <img class="event-image-thumb" src="images/thumbs/${tour.imageID}.jpg">
          </div>
          <div class="event-description-div">
              ${tour.tourName}
            <hr style="margin-right:5px; margin-top: 0px; margin-bottom: 0px;"></hr>
            <input class = "event-select-button" type="button" name = "view-tickets" value="View Tickets" onclick="location.href='artist?id=${artist.artistId}';">
            <p>
                ${tour.tourDescription}
            </p>
          </div>
        </li>
      </c:forEach>
    </ul>
  </div>
</div>
</body>
</html>
