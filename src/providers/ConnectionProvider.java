package providers;
        import java.sql.*;
        import java.util.logging.Logger;

        import static providers.Provider.*;

public class ConnectionProvider {
    private static Connection connection = null;


    static {
        try {
            Class.forName (DRIVER);
            connection = DriverManager.getConnection(CONNECTION_URL,USERNAME,PASSWORD);
        }   catch(Exception e){
            e.printStackTrace();
        }
    }

    public static Connection getConnection(){
        try {
            if(connection.isClosed()) {
                connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
            }
            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void closeConnection(){
        try {
            connection.close();
            connection = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
