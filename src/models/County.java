package models;


public class County {
    private int countyID;
    private String countyName;

    public int getCountyID() {return countyID; }

    public void setCountyID(int countyID) {
        this.countyID = countyID;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }
}
