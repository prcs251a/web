package models;

import java.util.Date;

public class Event {
    private int eventID;
    private Date eventDate;
    private boolean pastEvent;
    private String startTime;
    private String endTime;
    private int standingTickets;
    private int firstTierTickets;
    private int secondTierTickets;
    private int venueID;
    private int tourID;
    private Tour tour;
    private Venue venue;
    private boolean soldOut;
    private int imageID;

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public boolean isPastEvent() {
        return pastEvent;
    }

    public void setPastEvent(boolean pastEvent) {
        this.pastEvent = pastEvent;
    }

    public boolean isSoldOut() {
        return soldOut;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public void setSoldOut(boolean soldOut) {
        this.soldOut = soldOut;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getStandingTickets() {
        return standingTickets;
    }

    public void setStandingTickets(int standingTickets) {
        this.standingTickets = standingTickets;
    }

    public int getFirstTierTickets() {
        return firstTierTickets;
    }

    public void setFirstTierTickets(int firstTierTickets) {
        this.firstTierTickets = firstTierTickets;
    }

    public int getSecondTierTickets() {
        return secondTierTickets;
    }

    public void setSecondTierTickets(int secondTierTickets) {
        this.secondTierTickets = secondTierTickets;
    }

    public int getVenueID() {
        return venueID;
    }

    public void setVenueID(int venueID) {
        this.venueID = venueID;
    }

    public int getTourID() {
        return tourID;
    }

    public void setTourID(int tourID) {
        this.tourID = tourID;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }
}
