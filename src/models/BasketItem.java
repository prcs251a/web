package models;

public class BasketItem {
    private int itemID;
    private int eventID;
    private String tourName;
    private int tier;
    private String tierName;
    private int quantity;


    public BasketItem(int itemID, int eventID, int tier, int quantity){
        this.itemID = itemID;
        this.setEventID(eventID);
        this.setTier(tier);
        this.setQuantity(quantity);
    }


    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public int getTier() {
        return tier;
    }

    public void setTier(int tier) {
        this.tier = tier;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public String getTierName() {
        return tierName;
    }

    public void setTierName(String tierName) {
        this.tierName = tierName;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }
}
