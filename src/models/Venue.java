package models;

public class Venue {
    private int venueID;
    private String venueName;
    private String venueDescription;
    private int standingCapacity;
    private int firstTierCapacity;
    private int secondTierCapacity;
    private int countyID;
    private String city;
    private String addressLine1;
    private String addressLine2;
    private String postcode;
    private String telephoneNumber;
    private int imageID;

    public int getVenueID() {
        return venueID;
    }

    public void setVenueID(int venueID) {
        this.venueID = venueID;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVenueDescription() {
        return venueDescription;
    }

    public void setVenueDescription(String venueDescription) {
        this.venueDescription = venueDescription;
    }

    public int getStandingCapacity() {
        return standingCapacity;
    }

    public void setStandingCapacity(int standingCapacity) {
        this.standingCapacity = standingCapacity;
    }

    public int getCountyID() {
        return countyID;
    }

    public void setCountyID(int countyID) {
        this.countyID = countyID;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public int getFirstTierCapacity() {
        return firstTierCapacity;
    }

    public void setFirstTierCapacity(int firstTierCapacity) {
        this.firstTierCapacity = firstTierCapacity;
    }

    public int getSecondTierCapacity() {
        return secondTierCapacity;
    }

    public void setSecondTierCapacity(int secondTierCapacity) {
        this.secondTierCapacity = secondTierCapacity;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }
}
