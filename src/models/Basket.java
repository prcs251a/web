package models;
import java.util.ArrayList;

public class Basket {
    private ArrayList<BasketItem> basketItems = new ArrayList<>();

    public boolean addItem(int itemID, int tier, int eventID, int quantity){
        getBasketItems().add(new BasketItem(itemID, eventID, tier, quantity));
        return true;
    }

    public ArrayList<BasketItem> getBasketItems() {
        return basketItems;
    }

    public void setBasketItems(ArrayList<BasketItem> basketItems) {
        this.basketItems = basketItems;
    }
}
