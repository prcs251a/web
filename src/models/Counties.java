package models;

import java.util.ArrayList;

public class Counties {
    private ArrayList<County> counties = new ArrayList<>();

    public ArrayList<County> getCounties() {
        return counties;
    }

    public void setCounties(ArrayList<County> counties) {
        this.counties = counties;
    }
}
