package servlets;

import dao.CustomersDao;
import login.LoginDetails;
import login.PasswordUtil;
import login.RegisterDetails;
import models.Customer;
import models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/account")
public class AccountServlet extends HttpServlet {
    RegisterDetails details;
    User currentUser;
    Customer currentCustomer;


    @Override
    public void init() {
    }

    /**
     * Handles password change and account edits from the accounts page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        if(request.getParameter("action").equals("passwordChange")){
            LoginDetails details = new LoginDetails();
            details.setPassword(request.getParameter("currentPassword"));
            details.setUsername(currentUser.getUsername());
            if(CustomersDao.validate(details)) {
                try {
                    boolean status = CustomersDao.changePassword(currentUser.getUsername(), PasswordUtil.getSaltedHash(request.getParameter("newPassword")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else if(request.getParameter("action").equals("accountEdit")){
            Customer customer = new Customer();
            customer.setCustomerID(currentCustomer.getCustomerID());
            customer.setCustomerFirstName(request.getParameter("firstName"));
            customer.setCustomerLastName(request.getParameter("lastName"));
            customer.setAddressLine1(request.getParameter("addressLine1"));
            customer.setAddressLine2(request.getParameter("addressLine2"));
            customer.setCity(request.getParameter("city"));
            customer.setPostcode(request.getParameter("postCode"));
            customer.setCountyID(Integer.parseInt(request.getParameter("county")));
            customer.setCustomerEmail(request.getParameter("email1"));
            boolean status = CustomersDao.updateCustomer(customer);

            if(status){
                currentCustomer = customer;
                request.getSession().setAttribute("currentCustomer", currentCustomer);
            }
        }
        doGet(request, response);
    }

    /**
     * Prepares the necessary data and serves the account jsp file
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentUser = (User) request.getSession().getAttribute("currentUser");
        currentCustomer = (Customer) request.getSession().getAttribute("currentCustomer");
        request.getRequestDispatcher("/account.jsp").forward(request, response);
    }

}