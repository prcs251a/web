package servlets;


import dao.*;
import models.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/basket")
public class BasketServlet extends HttpServlet {
    Basket currentBasket;
    @Override
    public void init() {
    }

    /**
     * handles changing quantity and removal of basket items
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        if(request.getParameter("action").equals("quantity")) {
            int itemID = Integer.parseInt(request.getParameter("item"));
            int newQuantity = Integer.parseInt(request.getParameter("quantity"));
            if (currentBasket == null) currentBasket = new Basket();

            ArrayList<BasketItem> items = currentBasket.getBasketItems();
            for (BasketItem item : items) {
                if (item.getItemID() == itemID) {
                    if (newQuantity <= 4 && newQuantity > 0) {
                        item.setQuantity(newQuantity);
                    }
                }
            }
            request.getSession().setAttribute("currentBasket", currentBasket);
        }else{
            int itemID = Integer.parseInt(request.getParameter("item"));
            if(currentBasket == null)currentBasket = new Basket();
            for(int i = 0; i<currentBasket.getBasketItems().size(); i++) {
                if(currentBasket.getBasketItems().get(i).getItemID() == itemID){
                    currentBasket.getBasketItems().remove(i);
                    request.getSession().setAttribute("currentBasket", currentBasket);
                }
            }
        }
        doGet(request, response);
    }

    /**
     * prepares the data for and serves the basket jsp file
      * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentBasket = (Basket) request.getSession().getAttribute("currentBasket");

        for(BasketItem item : currentBasket.getBasketItems()){
            item.setTourName(TourDao.getTourByID(EventDao.getEventByID(item.getEventID()).getTourID()).getTourName());
            item.setTierName(SeatingTiersDao.getSeatingTierByID(item.getTier()).getTierName());
        }
        request.setAttribute("thisBasket", currentBasket);
        request.getRequestDispatcher("/basket.jsp").forward(request, response);
    }

}