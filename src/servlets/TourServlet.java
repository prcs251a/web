package servlets;

import dao.ArtistDao;
import dao.EventDao;
import dao.TourDao;
import dao.VenueDao;
import models.Artist;
import models.Event;
import models.Tour;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/tour")
public class TourServlet extends HttpServlet {

    @Override
    public void init() {
    }

    /**
     * prepares all the required data for the Tours page and serves the .jsp fil
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

            Tour tour = TourDao.getTourByID(Integer.parseInt(request.getParameter("id")));
            Artist artist = ArtistDao.getArtist(tour.getArtistID());
            ArrayList<Event> events = EventDao.getEventsByTourID(tour.getTourID());

            tour.setEvents(EventDao.getEventsByTourID(tour.getTourID()));
            for(Event event : tour.getEvents()) {
                event.setVenue(VenueDao.getVenue(event.getVenueID()));
            }

            request.setAttribute("artist", artist);
            request.setAttribute("events", events);
            request.setAttribute("tour", tour);
            request.getRequestDispatcher("/tour.jsp").forward(request, response);
    }

}