package servlets;

import dao.ArtistDao;
import dao.EventDao;
import dao.TourDao;
import dao.VenueDao;
import models.Artist;
import models.Event;
import models.Tour;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/artist")
public class ArtistServlet extends HttpServlet {

    @Override
    public void init() {
    }

    /**
     * Prepares the necessary data and serves the artist jsp page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            Artist artist = ArtistDao.getArtist(Integer.parseInt(request.getParameter("id")));
            ArrayList<Tour> tours = TourDao.getToursByArtistID(artist.getArtistId());

            for(Tour tour : tours){
                tour.setEvents(EventDao.getEventsByTourID(tour.getTourID()));
                for(Event event : tour.getEvents()){
                    event.setVenue(VenueDao.getVenue(event.getVenueID()));
                }
            }
            request.setAttribute("artist", artist);
            request.setAttribute("tours", tours);
            request.getRequestDispatcher("/artist.jsp").forward(request, response);
    }

}