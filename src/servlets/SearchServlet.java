package servlets;

import dao.*;
import models.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/search-results")
public class SearchServlet extends HttpServlet {

    @Override
    public void init() {
    }


    /**
     * Prepares the data for and serves the .jsp file for search-results
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<Artist> artists = ArtistDao.searchArtist(request.getParameter("q"));
        ArrayList<Venue> venues = VenueDao.searchVenue(request.getParameter("q"));
        ArrayList<Tour> tours = TourDao.searchTour(request.getParameter("q"));

        for(Venue venue : venues){
            venue.setImageID(VenueDao.getImageID(venue.getVenueID()));
        }
        request.setAttribute("artists", artists);
        request.setAttribute("venues", venues);
        request.setAttribute("tours", tours);
        request.getRequestDispatcher("/search-results.jsp").forward(request, response);
    }

}