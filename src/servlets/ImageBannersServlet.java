package servlets;

import providers.ConnectionProvider;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

@WebServlet("/images/banners/*")
public class ImageBannersServlet extends HttpServlet {

    /**
     * prepares images from the blob in the database based on the image name requested
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String imageName = request.getPathInfo().substring(1); // Returns "foo.png".

        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT IMAGEBANNER FROM IMAGES WHERE IMAGEID = ?");
            String imageID = imageName.substring(0,imageName.indexOf("."));
            stmt.setString(1, imageID);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Blob b = rs.getBlob("IMAGEBANNER");
                byte[] content = b.getBytes(1, (int) b.length());
                response.setContentType(getServletContext().getMimeType(imageName));
                response.setContentLength(content.length);
                response.getOutputStream().write(content);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}