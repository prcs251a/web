package servlets;

import dao.*;
import models.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/event")
public class EventServlet extends HttpServlet {
    Basket currentBasket;
    @Override
    public void init() {
    }

    /**
     * Handles adding tickets to the basket
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        SeatingTier tier = SeatingTiersDao.getSeatingTierByID(Integer.parseInt(request.getParameter("tier")));
        Event event = EventDao.getEventByID(Integer.parseInt(request.getParameter("event")));
        int addQuantity = Integer.parseInt(request.getParameter("quantity"));

        if(currentBasket == null)currentBasket = (Basket) request.getSession().getAttribute("currentBasket");

        if(currentBasket.getBasketItems().isEmpty()){
            currentBasket.addItem(currentBasket.getBasketItems().size()+1,tier.getTierID(), event.getEventID(), 1);
        }else {
            boolean modified = false;
            for (BasketItem item : currentBasket.getBasketItems()) {
                if (event.getEventID() == item.getEventID()) {
                    if (item.getTier() == tier.getTierID()) {
                        int quantity = item.getQuantity();
                        if(quantity < 4){
                            if(quantity + addQuantity <= 4) {
                                item.setQuantity(quantity + addQuantity);
                                modified = true;
                                break;
                            }else{
                                modified = true;
                            }
                        }else{
                            modified = true;
                        }
                    }
                }
            }
            if(!modified)currentBasket.addItem(currentBasket.getBasketItems().size()+1, tier.getTierID(), event.getEventID(), addQuantity);
        }
        if(currentBasket.getBasketItems().isEmpty())currentBasket.addItem(currentBasket.getBasketItems().size()+1, tier.getTierID(), event.getEventID(), addQuantity);
        request.getSession().setAttribute("currentBasket", currentBasket);
        doGet(request, response);
    }

    /**
     * prepares and serves the event jsp page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Event event = EventDao.getEventByID(Integer.parseInt(request.getParameter("id")));
        Tour tour = TourDao.getTourByID(event.getTourID());
        Artist artist = ArtistDao.getArtist(tour.getArtistID());

        request.setAttribute("artist", artist);
        request.setAttribute("tour", tour);
        request.setAttribute("event", event);
        request.getRequestDispatcher("/event.jsp").forward(request, response);
    }

}