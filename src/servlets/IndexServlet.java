package servlets;

import dao.*;
import models.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

@WebServlet("")
public class IndexServlet extends HttpServlet {
    Random r = new Random();

    @Override
    public void init() {
    }

    /**
     * Sets up required resources for the index page
     * serves the index page with the neccessary attributes
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<Artist> artists = ArtistDao.getAllArtists();
        ArrayList<Tour> tours = TourDao.getAllTours();
        ArrayList<Tour> selectedTours = new ArrayList<>();
        ArrayList<Artist> selectedArtists = new ArrayList<>();

        for(int i = 0; i < 3; i++){
            int j = r.nextInt(tours.size());
            selectedTours.add(tours.get(j));
            tours.remove(j);
            j = r.nextInt(artists.size());
            selectedArtists.add(artists.get(j));
            artists.remove(j);
        }

        request.setAttribute("artists", artists);
        request.setAttribute("selectedArtists", selectedArtists);
        request.setAttribute("tours", selectedTours);
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

}