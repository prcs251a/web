package servlets;

import dao.*;
import models.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/my-tickets")
public class HistoryServlet extends HttpServlet {
    Basket currentBasket;
    Customer currentCustomer;

    @Override
    public void init() {
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

    }

    /**
     * prepares and serves the history jsp page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentCustomer = (Customer) request.getSession().getAttribute("currentCustomer");

        if(currentCustomer != null) {
            if (currentCustomer.getCustomerID() != -1) {
                ArrayList<History> histories = BookingsDao.getHistoriesByCustomerID(currentCustomer.getCustomerID());

                for (History history : histories) {
                    for (Ticket ticket : history.getTickets()) {
                        SeatingTier tier = SeatingTiersDao.getSeatingTierByID(ticket.getTierID());
                        ticket.setTierName(tier.getTierName());
                        ticket.setTicketPrice(tier.getTicketPrice());
                    }
                }

                request.setAttribute("currentCustomer", currentCustomer);
                request.setAttribute("histories", histories);
                request.getRequestDispatcher("/my-tickets.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + "/");
            }
        } else {
            response.sendRedirect(request.getContextPath() + "/");
        }
    }

}