package servlets;

import dao.*;
import models.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/checkout")
public class CheckoutServlet extends HttpServlet {
    Basket currentBasket;
    User currentUser;
    Customer currentCustomer;

    @Override
    public void init() {
    }

    /**
     * Handles purchasing the items in the customer's basket
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        if(request.getParameter("action").equals("guest")) {
            currentCustomer = new Customer();
            currentCustomer.setCustomerID(CustomersDao.getNextCustomerID());
            currentCustomer.setAddressLine1(request.getParameter("addressLine1"));
            currentCustomer.setAddressLine2(request.getParameter("addressLine2"));
            currentCustomer.setPostcode(request.getParameter("postCode"));
            currentCustomer.setCity(request.getParameter("city"));
            currentCustomer.setCustomerEmail(request.getParameter("email1"));
            currentCustomer.setCustomerFirstName(request.getParameter("firstName"));
            currentCustomer.setCustomerLastName(request.getParameter("lastName"));
            currentCustomer.setCountyID(Integer.parseInt(request.getParameter("county")));
            CustomersDao.createCustomer(currentCustomer);

            request.getSession().setAttribute("currentCustomer", currentCustomer);

        }else if(request.getParameter("action").equals("purchase")){
            Booking booking = new Booking();
            if(currentUser.getUsername() == null){
                booking.setCustomerID(currentCustomer.getCustomerID());
            }else {
                booking.setCustomerID(currentCustomer.getCustomerID());
            }
            booking.setBookingID(BookingsDao.getNextBookingID());
            booking.setTotalCost(0f);
            boolean ticketStatus = false;
            for(BasketItem item : currentBasket.getBasketItems()){
                SeatingTier tier = SeatingTiersDao.getSeatingTierByID(item.getTier());
                booking.setTotalCost(booking.getTotalCost()+tier.getTicketPrice());
            }

            boolean bookingStatus = BookingsDao.createBooking(booking);

            for(BasketItem item : currentBasket.getBasketItems()){
                for(int i = 0; i < item.getQuantity(); i++) {
                    Ticket ticket = new Ticket();
                    ticket.setTicketID(TicketDao.getNextTicketID());
                    ticket.setTierID(item.getTier());
                    ticket.setBookingID(booking.getBookingID());
                    ticket.setEventID(item.getEventID());
                    ticketStatus = TicketDao.createTicket(ticket);
                }
            }

            if(bookingStatus && ticketStatus) {
                currentBasket = new Basket();
                request.getSession().setAttribute("currentBasket", currentBasket);
            }
        }
            doGet(request, response);

    }

    /**
     * prepares and serves the checkout jsp files depending on if there is a current user/customer
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentBasket = (Basket) request.getSession().getAttribute("currentBasket");
        currentUser = (User) request.getSession().getAttribute("currentUser");
        currentCustomer = (Customer) request.getSession().getAttribute("currentCustomer");

        if((currentUser.getCustomerID() != -1 || currentCustomer.getCustomerID() != -1) && currentCustomer != null) {
            if(currentBasket != null) {
                for (BasketItem item : currentBasket.getBasketItems()) {
                    item.setTourName(TourDao.getTourByID(EventDao.getEventByID(item.getEventID()).getTourID()).getTourName());
                    item.setTierName(SeatingTiersDao.getSeatingTierByID(item.getTier()).getTierName());
                }
            }
            request.setAttribute("thisBasket", currentBasket);
            request.getRequestDispatcher("/checkout.jsp").forward(request, response);
        }else{
            request.getRequestDispatcher("/checkout-auth.jsp").forward(request, response);
        }

    }

}