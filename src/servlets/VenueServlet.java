package servlets;

import dao.ArtistDao;
import dao.EventDao;
import dao.TourDao;
import dao.VenueDao;
import models.Artist;
import models.Event;
import models.Tour;
import models.Venue;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/venue")
public class VenueServlet extends HttpServlet {

    @Override
    public void init() {
    }

    /**
     * prepares and serves the venue jsp page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            Venue venue = VenueDao.getVenue(Integer.parseInt(request.getParameter("id")));
            venue.setImageID(VenueDao.getImageID(venue.getVenueID()));
            ArrayList<Event> events = EventDao.getEventsByVenue(venue.getVenueID());


            for(Event event : events){
                Tour tour = TourDao.getTourByID(event.getTourID());
                tour.setArtist(ArtistDao.getArtist(tour.getArtistID()));
                event.setTour(tour);
            }
            request.setAttribute("venue", venue);
            request.setAttribute("events", events);
            request.getRequestDispatcher("/venue.jsp").forward(request, response);
    }

}