package servlets;

import dao.*;
import login.PasswordUtil;
import login.RegisterDetails;
import models.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    RegisterDetails details;

    @Override
    public void init() {
    }

    /**
     * Handles registering a new customer
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        //Create User object
        User user = new User();
        user.setUserID(CustomersDao.getNextUserID());
        user.setUsername(request.getParameter("username"));
        user.setCustomerID(CustomersDao.getNextCustomerID());
        if (request.getParameter("password1").equals(request.getParameter("password2"))){

            try {
                String pass = PasswordUtil.getSaltedHash(request.getParameter("password1"));
                user.setPassword(pass);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //user.setPassword(request.getParameter("password1"));
        }

        user.setRoleID(3);

        if (request.getParameter("email1").equals(request.getParameter("email2"))) {
            user.setEmail(request.getParameter("email1"));
        }
        //End of User Object
        //Create Customer Object
        Customer customer = new Customer();
        customer.setCustomerID(CustomersDao.getNextCustomerID());
        customer.setAddressLine1(request.getParameter("addressLine1"));
        customer.setAddressLine2(request.getParameter("addressLine2"));
        customer.setPostcode(request.getParameter("postCode"));
        customer.setCity(request.getParameter("city"));
        customer.setCustomerEmail(request.getParameter("email1"));
        customer.setCustomerFirstName(request.getParameter("firstName"));
        customer.setCustomerLastName(request.getParameter("lastName"));
        customer.setCountyID(Integer.parseInt(request.getParameter("county")));
        //End of Customer Object

        boolean status = CustomersDao.register(user, customer);

        if (status) {
            request.getSession().setAttribute("session", "TRUE");
            User userd = CustomersDao.getUserByUsername(request.getParameter("username"));
            Customer customerd = CustomersDao.getCustomerByID(user.getCustomerID());

            request.getSession().setAttribute("currentUser", userd);
            request.getSession().setAttribute("currentCustomer", customerd);
        }
        doGet(request, response);

    }

    /**
     * prepares and serves the register jsp page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/register.jsp").forward(request, response);
    }

}