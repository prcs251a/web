package login;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.SecureRandom;
import java.util.Base64;

public class PasswordUtil {
    private static final int iterations = 25*1000;
    private static final int saltLength = 32;
    private static final int keyLength = 256;

    /**
     * getSaltedHash
     * @param password password to hash
     * @return String representing the hashed and salted password in the format salt$hash
     * @throws Exception
     */
    public static String getSaltedHash(String password) throws Exception {
        byte[] salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(saltLength);
        return Base64.getEncoder().encodeToString(salt) + "$" + hash(password, salt);
    }

    /**
     * checks a plaintext password against the hash stored in the database
     * @param password plain text password to check
     * @param stored the stored salted hash in the format salt$hash
     * @return
     * @throws Exception
     */
    public static boolean check(String password, String stored) throws Exception{
        String[] saltAndPass = stored.split("\\$");
        if (saltAndPass.length != 2) {
            throw new IllegalStateException("incorrect format");
        }
        String hashOfInput = hash(password, Base64.getDecoder().decode(saltAndPass[0]));
        return hashOfInput.equals(saltAndPass[1]);
    }

    /**
     * hash
     * @param password password to hash
     * @param salt salt to hash with
     * @return String representing the hashed and salted password without the salt concatenated
     * @throws Exception
     */
    private static String hash(String password, byte[] salt) throws Exception {
        if (password == null || password.length() == 0)
            throw new IllegalArgumentException("Password was empty");
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(
                password.toCharArray(), salt, iterations, keyLength)
        );
        return Base64.getEncoder().encodeToString(key.getEncoded());
    }
}