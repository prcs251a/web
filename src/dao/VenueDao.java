package dao;

import models.Venue;
import providers.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class VenueDao {

    /**
     * getVenue
     * @param venueID the desired venueID
     * @return returns the venue with the given ID
     */
    public static Venue getVenue(int venueID) {
        Venue venue = new Venue();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM VENUES WHERE VENUEID=?");
            stmt.setInt(1, venueID);
            ResultSet rs = stmt.executeQuery();
            rs.next();

            venue.setVenueID(venueID);
            venue.setAddressLine1(rs.getString("ADDRESSLINE1"));
            venue.setAddressLine2(rs.getString("ADDRESSLINE2"));
            venue.setCity(rs.getString("CITY"));
            venue.setCountyID(rs.getInt("COUNTYID"));
            venue.setPostcode(rs.getString("POSTCODE"));
            venue.setStandingCapacity(rs.getInt("STANDINGCAPACITY"));
            venue.setFirstTierCapacity(rs.getInt("FIRSTTIERCAPACITY"));
            venue.setSecondTierCapacity(rs.getInt("SECONDTIERCAPACITY"));
            venue.setVenueDescription(rs.getString("VENUEDESCRIPTION"));
            venue.setVenueName(rs.getString("VENUENAME"));
            venue.setTelephoneNumber(rs.getString("TELEPHONENUMBER"));
            stmt.close();
            rs.close();

        } catch (Exception e) {
            e.printStackTrace();
        }finally{

        }
        return venue;
    }

    /**
     * getImageID
     * @param venueID desired venue ID
     * @return returns the Image ID associated with the given venue ID
     */
    public static int getImageID(int venueID){
        int imageID = 0;
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT IMAGEID FROM VENUES WHERE VENUEID =?");
            stmt.setInt(1,venueID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            imageID = rs.getInt("IMAGEID");
            stmt.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return imageID;
    }

    /**
     * searchVenue
     * @param venueName string of partial name to search for
     * @return ArrayList representing the venues that match the given partial string
     */
    public static ArrayList<Venue> searchVenue(String venueName){
        ArrayList<Venue> venues = new ArrayList<>();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM VENUES WHERE UPPER(VENUENAME) LIKE UPPER(?)");
            stmt.setString(1, "%" + venueName + "%");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                Venue venue = new Venue();
                venue.setVenueID(rs.getInt("VENUEID"));
                venue.setAddressLine1(rs.getString("ADDRESSLINE1"));
                venue.setAddressLine2(rs.getString("ADDRESSLINE2"));
                venue.setCity(rs.getString("CITY"));
                venue.setCountyID(rs.getInt("COUNTYID"));
                venue.setPostcode(rs.getString("POSTCODE"));
                venue.setStandingCapacity(rs.getInt("STANDINGCAPACITY"));
                venue.setFirstTierCapacity(rs.getInt("FIRSTTIERCAPACITY"));
                venue.setSecondTierCapacity(rs.getInt("SECONDTIERCAPACITY"));
                venue.setVenueDescription(rs.getString("VENUEDESCRIPTION"));
                venue.setVenueName(rs.getString("VENUENAME"));
                venue.setTelephoneNumber(rs.getString("TELEPHONENUMBER"));
                venues.add(venue);
            }
            stmt.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return venues;
    }

}
