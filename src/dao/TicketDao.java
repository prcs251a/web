package dao;

import models.Booking;
import models.Ticket;
import providers.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class TicketDao {

    /**
     * getNextTicketID
     * @return int representing the next available ticket ID in the table
     */
    public static int getNextTicketID() {
        int count = -1;
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT MAX(TICKETID) AS rowcount FROM TICKETS");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            count = rs.getInt("rowcount") + 1;
            rs.close();
            stmt.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * createTicket
     * @param ticket ticket object to create in the database
     * @return boolean indicating success of creation
     */
    public static boolean createTicket(Ticket ticket){
        boolean status = false;
        try {
            Connection connection = ConnectionProvider.getConnection();

            PreparedStatement stmtCustomer = connection.prepareStatement("INSERT INTO TICKETS VALUES(?,?,?,?) ");
            stmtCustomer.setInt(1, ticket.getTicketID());
            stmtCustomer.setInt(2, ticket.getTierID());
            stmtCustomer.setInt(3, ticket.getBookingID());
            stmtCustomer.setInt(4, ticket.getEventID());

            ResultSet rsCustomer = stmtCustomer.executeQuery();
            status = rsCustomer.next();
            rsCustomer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }
}
