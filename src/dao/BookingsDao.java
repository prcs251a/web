package dao;

import models.Booking;
import models.History;
import models.Ticket;
import providers.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class BookingsDao {

    /**
     * getBookingsByCustomerID
     * @param customerID the desired customer ID
     * @return ArrayList of all bookings associated with the given customer ID
     */
    public static ArrayList<Booking> getBookingsByCustomerID(int customerID){
        ArrayList<Booking> bookings = new ArrayList<>();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM BOOKINGS WHERE CUSTOMERID=?");
            stmt.setInt(1, customerID);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Booking booking = new Booking();
                booking.setCustomerID(rs.getInt("CUSTOMERID"));
                booking.setBookingID(rs.getInt("BOOKINGID"));
                booking.setDatePurchased(rs.getString("DATEPURCHASED"));
                booking.setTotalCost(rs.getInt("TOTALCOST"));
                bookings.add(booking);
            }
            rs.close();
            stmt.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return bookings;
    }

    /**
     * getTicketByBookingID
     * @param bookingID the desired booking ID
     * @return ArrayList of all tickets associated with the given booking ID
     */
    public static ArrayList<Ticket> getTicketsByBookingID(int bookingID){
        ArrayList<Ticket> tickets = new ArrayList<>();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM TICKETS WHERE BOOKINGID=?");
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Ticket ticket = new Ticket();
                ticket.setBookingID(rs.getInt("BOOKINGID"));
                ticket.setEventID(rs.getInt("EVENTID"));
                ticket.setTicketID(rs.getInt("TICKETID"));
                ticket.setTierID(rs.getInt("TIERID"));
                tickets.add(ticket);
            }
            rs.close();
            stmt.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return tickets;
    }

    /**
     * getHistoriesByCustomerID
     * @param customerID the desired customer ID
     * @return ArrayList of 'histories' associated with the customer - contains the bookings and tickets for ease of access
     */
    public static ArrayList<History> getHistoriesByCustomerID(int customerID){
        ArrayList<History> histories = new ArrayList<>();

        ArrayList<Booking> bookings = getBookingsByCustomerID(customerID);
        for(Booking booking : bookings){
            History history = new History();
            history.setBooking(booking);
            history.setTickets(getTicketsByBookingID(booking.getBookingID()));
            history.setTourName(TourDao.getTourByID(EventDao.getEventByID(history.getTickets().get(0).getEventID()).getTourID()).getTourName());
            histories.add(history);
        }

        return histories;
    }

    /**
     * createBooking
     * @param booking the Booking object to create
     * @return boolean indicating success of creating booking
     */
    public static boolean createBooking(Booking booking){
        boolean status = false;
        try {
            Connection connection = ConnectionProvider.getConnection();

            PreparedStatement stmtCustomer = connection.prepareStatement("INSERT INTO BOOKINGS VALUES(?,?,SYSDATE,?) ");
            stmtCustomer.setInt(1, booking.getBookingID());
            stmtCustomer.setInt(2, booking.getCustomerID());
            stmtCustomer.setFloat(3, booking.getTotalCost());

            ResultSet rsCustomer = stmtCustomer.executeQuery();
            status = rsCustomer.next();
            rsCustomer.close();
            stmtCustomer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    /**
     * getNextBookingID
     * @return int of the next available ID in th bookings table
     */
    public static int getNextBookingID() {
        int count = -1;
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT MAX(BOOKINGID) AS rowcount FROM BOOKINGS");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            count = rs.getInt("rowcount") + 1;
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }
}
