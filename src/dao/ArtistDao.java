package dao;
import providers.ConnectionProvider;
import models.Artist;

import java.sql.*;
import java.util.ArrayList;

public class ArtistDao {


    /**
     * getArtist
     * @param artistId
     * @return retuens the artist with the specified ID
     */
    public static Artist getArtist(int artistId) {
        Artist artist = new Artist();

        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ARTISTS WHERE ARTISTID=?");

            stmt.setInt(1, artistId);

            ResultSet rs = stmt.executeQuery();
            rs.next();
            artist.setArtistId(artistId);
            artist.setArtistName(rs.getString("ARTISTNAME"));
            System.out.println("test");
            artist.setArtistDescription(rs.getString("ARTISTDESCRIPTION"));
            artist.setGenreId(rs.getInt("GENREID"));
            artist.setImageID(rs.getInt("IMAGEID"));
            stmt.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
            return artist;
    }

    /**
     * getAllArtists
     * @return all artists in the table
     */
    public static ArrayList<Artist> getAllArtists(){
        ArrayList<Artist> artists = new ArrayList<>();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ARTISTS ");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Artist artist = new Artist();
                artist.setArtistId(rs.getInt("ARTISTID"));
                artist.setArtistName(rs.getString("ARTISTNAME"));
                System.out.println("test");
                artist.setArtistDescription(rs.getString("ARTISTDESCRIPTION"));
                artist.setGenreId(rs.getInt("GENREID"));
                artist.setImageID(rs.getInt("IMAGEID"));
                artists.add(artist);
            }
            stmt.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return artists;
    }

    /**
     * searchArtist
     * @param artistName String containing the partial artistName you're searching for
     * @return list of all artists which match the partial string
     */
    public static ArrayList<Artist> searchArtist(String artistName){
        ArrayList<Artist> artists = new ArrayList<>();
        /*artistName = artistName
                .replace("!", "!!")
                .replace("%", "!%")
                .replace("_", "!_")
                .replace("[", "![");*/
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ARTISTS WHERE UPPER(ARTISTNAME) LIKE UPPER(?)");
            stmt.setString(1, "%" + artistName + "%");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                Artist artist = new Artist();
                artist.setArtistId(rs.getInt("ARTISTID"));
                artist.setArtistName(rs.getString("ARTISTNAME"));
                artist.setArtistDescription(rs.getString("ARTISTDESCRIPTION"));
                artist.setGenreId(rs.getInt("GENREID"));
                artist.setImageID(rs.getInt("IMAGEID"));
                artists.add(artist);
            }
            stmt.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return artists;
    }

}
