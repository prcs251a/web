package dao;

import models.Tour;
import providers.ConnectionProvider;

import java.sql.*;
import java.util.ArrayList;


public class TourDao {

    /**
     * getToursByArtistID
     * @param artistID
     * @return returns all the tours related to the given artist ID
     */
    public static ArrayList<Tour> getToursByArtistID(int artistID) {
        ArrayList<Tour> tours = new ArrayList<>();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM TOURS WHERE ARTISTID=?");
            stmt.setInt(1, artistID);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                Tour tour = new Tour();
                tour.setArtistID(artistID);
                tour.setTourID(rs.getInt("TOURID"));
                tour.setStartDate(rs.getDate("STARTDATE"));
                tour.setEndDate(rs.getDate("ENDDATE"));
                tour.setTourDescription(rs.getString("TOURDESCRIPTION"));
                tour.setTourName(rs.getString("TOURNAME"));
                tour.setImageID(rs.getInt("IMAGEID"));
                tours.add(tour);
            }
            stmt.close();
            rs.close();
        } catch (Exception e) {
                e.printStackTrace();
            }
        return tours;
    }

    /**
     * searchTour
     * @param tourName string of the partial tourName you're searching for
     * @return list of all tours with names matching the partial
     */
    public static ArrayList<Tour> searchTour(String tourName){
        ArrayList<Tour> tours = new ArrayList<>();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM TOURS WHERE UPPER(TOURNAME) LIKE UPPER(?)");
            stmt.setString(1, "%" + tourName + "%");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                Tour tour = new Tour();
                tour.setTourID(rs.getInt("TOURID"));
                tour.setArtistID(rs.getInt("ARTISTID"));
                tour.setTourName(rs.getString("TOURNAME"));
                tour.setStartDate(rs.getDate("STARTDATE"));
                tour.setEndDate(rs.getDate("ENDDATE"));
                tour.setImageID(rs.getInt("IMAGEID"));
                tour.setTourDescription(rs.getString("TOURDESCRIPTION"));
                tours.add(tour);
            }
            stmt.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tours;
    }

    /**
     * getAllTours
     * @return ArrayList<Tour> list of all tours in the database
     */
    public static ArrayList<Tour> getAllTours(){
        ArrayList<Tour> tours = new ArrayList<>();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM TOURS");
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                Tour tour = new Tour();
                tour.setArtistID(rs.getInt("ARTISTID"));
                tour.setTourID(rs.getInt("TOURID"));
                tour.setStartDate(rs.getDate("STARTDATE"));
                tour.setEndDate(rs.getDate("ENDDATE"));
                tour.setImageID(rs.getInt("IMAGEID"));
                tour.setTourDescription(rs.getString("TOURDESCRIPTION"));
                tour.setTourName(rs.getString("TOURNAME"));
                tours.add(tour);
            }
            stmt.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tours;
    }

    /**
     * getTouByID
     * @param tourID
     * @return Tour the tour associated with the given tour ID
     */
    public static Tour getTourByID(int tourID){
        Tour tour = new Tour();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM TOURS WHERE TOURID=?");
            stmt.setInt(1, tourID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            tour.setTourID(rs.getInt("TOURID"));
            tour.setArtistID(rs.getInt("ARTISTID"));
            tour.setTourName(rs.getString("TOURNAME"));
            tour.setStartDate(rs.getDate("STARTDATE"));
            tour.setEndDate(rs.getDate("ENDDATE"));
            tour.setImageID(rs.getInt("IMAGEID"));
            tour.setTourDescription(rs.getString("TOURDESCRIPTION"));
            stmt.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tour;
    }
}
