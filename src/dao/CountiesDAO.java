package dao;

import models.Counties;
import models.County;
import providers.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CountiesDao {

    /**
     * getCounties
     * @return ArrayList of counties
     */
    public static ArrayList<County> getCounties(){
        ArrayList<County> counties = new ArrayList<>();

        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM COUNTIES");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                County county = new County();
                county.setCountyID(rs.getInt("COUNTYID"));
                county.setCountyName(rs.getString("COUNTYNAME"));
                counties.add(county);
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return counties;
    }
}
