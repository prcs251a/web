package dao;

import models.SeatingTier;
import providers.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SeatingTiersDao {

    /**
     * getSeatingTierByID
     * @param seatingTierID desiredTierID
     * @return SeatingTier object representing the tier with the given ID
     */
    public static SeatingTier getSeatingTierByID(int seatingTierID){
        SeatingTier seatingTier = new SeatingTier();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM SEATINGTIERS WHERE TIERID=?");

            stmt.setInt(1, seatingTierID);

            ResultSet rs = stmt.executeQuery();
            rs.next();
            seatingTier.setTierID(rs.getInt("TIERID"));
            seatingTier.setTicketPrice(Float.parseFloat(rs.getString("TICKETPRICE")));
            seatingTier.setTierName(rs.getString("TIERNAME"));
            stmt.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return seatingTier;
    }

}
