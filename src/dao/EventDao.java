package dao;

import models.Event;
import providers.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class EventDao {

    /**
     * getEventsByTourID
     * @param tourID desired Tour ID
     * @return ArrayList containing all the events associated with that tour
     */
    public static ArrayList<Event> getEventsByTourID(int tourID) {
        ArrayList<Event> events = new ArrayList<>();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM EVENTS WHERE TOURID=?");
            stmt.setInt(1, tourID);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                Event event = new Event();
                event.setTourID(tourID);
                event.setEventID(rs.getInt("EVENTID"));
                event.setEndTime(rs.getString("ENDTIME"));
                event.setEventDate(rs.getDate("EVENTDATE"));
                if(event.getEventDate().before(new Date()))event.setPastEvent(true);
                event.setFirstTierTickets(rs.getInt("FIRSTTIERTICKETS"));
                event.setSecondTierTickets(rs.getInt("SECONDTIERTICKETS"));
                event.setStandingTickets(rs.getInt("STANDINGTICKETS"));
                event.setStartTime(rs.getString("STARTTIME"));
                event.setVenueID(rs.getInt("VENUEID"));
                if(event.getStandingTickets() == 0 && event.getFirstTierTickets() == 0 && event.getSecondTierTickets() == 0){
                    event.setSoldOut(true);
                }
                events.add(event);
            }
            stmt.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return events;
    }

    /**
     *
     * @param venueID desired venue ID
     * @return ArrayList containing all the events associated with the given venue
     */
    public static ArrayList<Event> getEventsByVenue(int venueID){
        ArrayList<Event> events = new ArrayList<>();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM EVENTS WHERE VENUEID=?");
            stmt.setInt(1, venueID);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                Event event = new Event();
                event.setTourID(rs.getInt("TOURID"));
                event.setEventID(rs.getInt("EVENTID"));
                event.setEndTime(rs.getString("ENDTIME"));
                event.setEventDate(rs.getDate("EVENTDATE"));
                if(event.getEventDate().before(new Date()))event.setPastEvent(true);
                event.setFirstTierTickets(rs.getInt("FIRSTTIERTICKETS"));
                event.setSecondTierTickets(rs.getInt("SECONDTIERTICKETS"));
                event.setStandingTickets(rs.getInt("STANDINGTICKETS"));
                event.setStartTime(rs.getString("STARTTIME"));
                event.setVenueID(rs.getInt("VENUEID"));
                if(event.getStandingTickets() == 0 && event.getFirstTierTickets() == 0 && event.getSecondTierTickets() == 0){
                    event.setSoldOut(true);
                }
                events.add(event);
            }
            stmt.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return events;
    }

    /**
     * getEventByID
     * @param eventID desired eventID
     * @return Event representing the event with the given eventID
     */
    public static Event getEventByID(int eventID){
        Event event = new Event();

        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM EVENTS WHERE EVENTID=?");
            stmt.setInt(1, eventID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            event.setEventID(rs.getInt("EVENTID"));
            event.setEventDate(rs.getDate("EVENTDATE"));
            if(event.getEventDate().before(new Date()))event.setPastEvent(true);
            event.setStartTime(rs.getString("STARTTIME"));
            event.setEndTime(rs.getString("ENDTIME"));
            event.setStandingTickets(rs.getInt("STANDINGTICKETS"));
            event.setFirstTierTickets(rs.getInt("FIRSTTIERTICKETS"));
            event.setSecondTierTickets(rs.getInt("SECONDTIERTICKETS"));
            if(event.getStandingTickets() == 0 && event.getFirstTierTickets() == 0 && event.getSecondTierTickets() == 0){
                event.setSoldOut(true);
            }
            event.setVenueID(rs.getInt("VENUEID"));
            event.setTourID(rs.getInt("TOURID"));
            stmt.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return event;
    }
}
