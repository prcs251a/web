package dao;
import login.PasswordUtil;
import models.Customer;
import providers.ConnectionProvider;
import login.LoginDetails;
import models.User;

import java.sql.*;

public class CustomersDao {

    /**
     * getNextUserID
     * @return int of next available ID in users table
     */
    public static int getNextUserID(){
        int count = -1;
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT MAX(USERID) AS maxid FROM USERS");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            count = rs.getInt("maxid") + 1;
            rs.close();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * createCustomer
     * @param customer customer object to create in the database
     * @return boolean indicating success of creation in database
     */
    public static boolean createCustomer(Customer customer){
        boolean customerStatus = false;
        try {
            Connection connection = ConnectionProvider.getConnection();

            PreparedStatement stmtCustomer = connection.prepareStatement("INSERT INTO CUSTOMERS VALUES(?,?,?,?,?,?,?,?,?) ");
            stmtCustomer.setInt(1, customer.getCustomerID());
            stmtCustomer.setString(2, customer.getCustomerFirstName());
            stmtCustomer.setString(3, customer.getCustomerLastName());
            stmtCustomer.setString(4, customer.getCustomerEmail());
            stmtCustomer.setInt(5, customer.getCountyID());
            stmtCustomer.setString(6, customer.getCity());
            stmtCustomer.setString(7, customer.getAddressLine1());
            stmtCustomer.setString(8, customer.getAddressLine2());
            stmtCustomer.setString(9, customer.getPostcode());
            ResultSet rsCustomer = stmtCustomer.executeQuery();
            customerStatus = rsCustomer.next();
            rsCustomer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(customerStatus)return true;
        else return false;
    }

    /**
     * createUser
     * @param user user object to create
     * @return boolean indicating success of creation in database
     */
    public static boolean createUser(User user){
        boolean userStatus = false;
        try {
            Connection connection = ConnectionProvider.getConnection();

            PreparedStatement stmtUser = connection.prepareStatement("INSERT INTO USERS VALUES(?,?,?,?,?,?) ");
            stmtUser.setInt(1, user.getUserID());
            stmtUser.setString(2, user.getUsername());
            stmtUser.setString(3, user.getPassword());
            stmtUser.setInt(4, user.getCustomerID());
            stmtUser.setInt(5, user.getRoleID());
            stmtUser.setString(6, user.getEmail());
            ResultSet rsUser = stmtUser.executeQuery();
            userStatus = rsUser.next();
            rsUser.close();
            stmtUser.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
        if(userStatus)return true;
        else return false;
    }


    /**
     * updateCustomer
     * @param customer customer object containing updated information
     * @return boolean indecating success of update
     */
    public static boolean updateCustomer(Customer customer){
        boolean customerStatus = false;
                try{
                    Connection connection = ConnectionProvider.getConnection();
                    PreparedStatement stmtCustomer = connection.prepareStatement("UPDATE CUSTOMERS SET CUSTOMERFIRSTNAME=?,CUSTOMERLASTNAME = ?, CUSTOMEREMAIL=?, COUNTYID=?, CITY=?,ADDRESSLINE1=?,ADDRESSLINE2=?,POSTCODE=? WHERE CUSTOMERID = ?");
                    stmtCustomer.setString(1, customer.getCustomerFirstName());
                    stmtCustomer.setString(2, customer.getCustomerLastName());
                    stmtCustomer.setString(3,customer.getCustomerEmail());
                    stmtCustomer.setInt(4, customer.getCountyID());
                    stmtCustomer.setString(5,customer.getCity());
                    stmtCustomer.setString(6, customer.getAddressLine1());
                    stmtCustomer.setString(7, customer.getAddressLine2());
                    stmtCustomer.setString(8,customer.getPostcode());
                    stmtCustomer.setInt(9, customer.getCustomerID());

                    ResultSet rs = stmtCustomer.executeQuery();
                    customerStatus = rs.next();
                    stmtCustomer.close();
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        return customerStatus;
    }


    /**
     * changePassword
     * @param username username of the User to update
     * @param password salt$hash of the new password to store
     * @return boolean indicating success
     */
    public static boolean changePassword(String username, String password){
        boolean userStatus = false;
        try{
        Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmtUser = connection.prepareStatement("UPDATE USERS SET PASSWORD = (?) WHERE USERNAME = (?)");
            stmtUser.setString(1,password);
            stmtUser.setString(2, username);
            ResultSet rs = stmtUser.executeQuery();
            stmtUser.close();
            userStatus = rs.next();
            rs.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return userStatus;
    }

    /**
     * register
     * @param user user to add to the database
     * @param customer customer to add to the database
     * @return boolean indeicating success
     */
    public static boolean register(User user, Customer customer){
        boolean userStatus = false;
        boolean customerStatus = false;

        try {
            Connection connection = ConnectionProvider.getConnection();
            customerStatus = createCustomer(customer);
            userStatus = createUser(user);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if(customerStatus && userStatus)return true;
        else return false;
    }

    /**
     * validate
     * @param details login details to check against the datbase
     * @return boolean indicating if the user ir validated
     */
    public static boolean validate(LoginDetails details) {
        boolean status = false;

        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Users WHERE USERNAME=?");

            stmt.setString(1, details.getUsername());
            ResultSet rs = stmt.executeQuery();
            rs.next();
            status = PasswordUtil.check(details.getPassword(), rs.getString("PASSWORD"));
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    /**
     * getUserByUsername
     * @param username the username of the desired user
     * @return User object of the user with the requested username
     */
    public static User getUserByUsername(String username){
        User user = new User();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM USERS WHERE USERNAME = ?");
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                user.setUserID(rs.getInt("USERID"));
                user.setUsername(rs.getString("USERNAME"));
                user.setEmail(rs.getString("EMAIL"));
                user.setUserID(rs.getInt("USERID"));
                user.setCustomerID(rs.getInt("CUSTOMERID"));
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * getCustomerByID
     * @param customerID desired customer ID
     * @return Customer object representing the customer with the requested ID
     */
    public static Customer getCustomerByID(int customerID){
        Customer customer = new Customer();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM CUSTOMERS WHERE CUSTOMERID = ?");
            stmt.setInt(1, customerID);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                customer.setCustomerID(rs.getInt("CUSTOMERID"));
                customer.setCustomerFirstName(rs.getString("CUSTOMERFIRSTNAME"));
                customer.setCustomerLastName(rs.getString("CUSTOMERLASTNAME"));
                customer.setCountyID(rs.getInt("COUNTYID"));
                customer.setAddressLine1(rs.getString("ADDRESSLINE1"));
                customer.setAddressLine2(rs.getString("ADDRESSLINE2"));
                customer.setPostcode(rs.getString("POSTCODE"));
                customer.setCity(rs.getString("CITY"));
                customer.setCustomerEmail(rs.getString("CUSTOMEREMAIL"));
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customer;
    }

    /**
     * getNextCustomerID
     * @return int representing the next available customer ID
     */
    public static int getNextCustomerID() {
        int count = -1;
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT MAX(CUSTOMERID) AS rowcount FROM CUSTOMERS");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            count = rs.getInt("rowcount") + 1;
            rs.close();
            stmt.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }


}
